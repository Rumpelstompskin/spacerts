﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAI : Unit
{
    [SerializeField] AudioClip missileLaunchSFX;
    [SerializeField] AudioClip vesselExplosionSFX;

    [SerializeField] public Layer enemyLayer; // Specify which layer to see as enemy.

    [SerializeField] GameObject ShotPrefab; // Currently placeholder for MissilePrefab.
    [SerializeField] GameObject EmittionPoint; // PlaceHolder for where the missile launches from.
    [SerializeField] GameObject DeathPrefab; // Death particle prefab;

    Rigidbody rb; // Local rigidbody reference. Initiated in start.
    public GameObject EnemyToAttack; // The unit we should be attacking.
    public bool underAttack = false; // Not doing anything just yet. Idea is to make the shit attack back if struck.


    public Vector3 TargetPos; // The targeted possition our player has sent the units.
    public float RotationSpeed; // The rotation speed of our units. TODO Add mass, inertia and actual math to make movement more reliable. AKA redo this shit.
    public float MovementSpeed; // " "

    public float StopDistance = 10f; // Dictates when the vessel should stop moving forward to stop.
    public float RoamCheckDelay = 10f; // Time between overlapshere calls.

    public float AttackDistance; // How far away the unit can shoot.
    public float AttackDamage; // How much damage our unit should deal?
    public float AttackRate; // How fast does our target shoot? In seconds.
    public float AttackType; // TODO add attack types. Change to an enum?

    public float VesselHealth; // Health of our vessel. TODO Add shields.

    private float shotDelay = 0f; // Does not actually control the delay between shots. Needs to be reworked, changing the value of this should yield no change to anything.

    public bool moveOrderGiven = false; // Has this unit been ordered to move?
    public bool attackorderGiven = false;

    private List<GameObject> scannedtargets;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        scannedtargets = new List<GameObject>();
        if (enemyLayer == Layer.Enemy)
        {
            PlayerData data = Camera.main.GetComponentInParent<PlayerData>();
            if (data != null)
            {
                if (!data._playerOwnedTargets.Contains(gameObject))
                {
                    data._playerOwnedTargets.Add(gameObject);
                }
            }
        }
    }

    private void Update()
    {
        if (moveOrderGiven == true) // Self-explanatory
        {
            Vector3 offset = TargetPos - transform.position; // Get a direction which we use as an offset.
            float sqrLenght = offset.sqrMagnitude; // Square magnitude = Square lenth.

            if (sqrLenght < StopDistance * StopDistance) // when it's smaller than our stop distance2 then stop.
            {
                moveOrderGiven = false;
            }
        }

        //ScanForTargets(); // Checks for targets every RoamCheckDelay amount of time. Will only work if no order has been given and has no current target.
        ScanForTargets2();
    }

    void FixedUpdate()
    {
        if (moveOrderGiven == true) // If we have given an order to move, move and do nothing else.
        {
            MoveUnitRB(TargetPos, RotationSpeed, MovementSpeed, rb);
            return;
        }

        if(attackorderGiven == true)
        {
            MoveUnitRB(TargetPos, RotationSpeed, MovementSpeed, rb);
        }

        if (moveOrderGiven == false && EnemyToAttack != null) // This allows our unit to chase an enemy target.
        {
            //Vector3 Offset = new Vector3(EnemyToAttack.transform.localPosition.x + Random.Range(-100f,100f), EnemyToAttack.transform.localPosition.y, EnemyToAttack.transform.localPosition.z + Random.Range(-100f, 100f));
            //MoveUnit(EnemyToAttack.transform.localPosition - transform.position, 1f, 3f);
            MoveUnitRB(EnemyToAttack.transform.position - transform.position, RotationSpeed, MovementSpeed, rb); // By feeding the direction instead of the position, we make our units turn a little slower in combat, which makes the dogfighting more believable.
        }
        if(EnemyToAttack == null)
        {
            return;
        }

        if (shotDelay <= 0f)
        {
            if (Vector3.Distance(EnemyToAttack.transform.position, transform.position) <= AttackDistance)
            {
                MissileAttack(EnemyToAttack, gameObject, rb, ShotPrefab, EmittionPoint, AttackDistance, AttackDamage);
                shotDelay = Random.Range(AttackRate - Random.Range(0.1f, 0.5f), AttackRate);
                shotDelay -= Time.deltaTime;
            }
        }
        else
        {
            shotDelay -= Time.deltaTime;
        }
    }

    private void ScanForTargets()
    {
        if (RoamCheckDelay > 0f) { RoamCheckDelay -= Time.deltaTime; return; } else { RoamCheckDelay = Random.Range(1f, 2f); RoamCheckDelay -= Time.deltaTime; }
        if(moveOrderGiven == false && EnemyToAttack == null) // Is the unit on it's way somewhere and do we have a target to attack?
        {
            int layerMask = 1 << (int)enemyLayer; // Bit shift our layer variable. This tells the unit which layer to consider an enemy.
            Collider[] sphere = Physics.OverlapSphere(transform.position, AttackDistance, layerMask); // We instantiate a Collider array variable and cast a sphere at the unit's position with the unit's attack distance as range on the Enemy Layer.

            foreach (Collider enemy in sphere) // Loop through the results and the first unit we get that isn't null is targeted. Could be changed to just target the first member of that array if it's not null. Could save some cpu there.
            {
                if(enemy != null) // Null filter check
                {
                    EnemyToAttack = enemy.gameObject; // Giving ourselves a target.
                    break; // Stopping the loop from going any further and since we just gave our unit a target this function won't run again until we need a unit.
                }
            }
        }
    }

    private void ScanForTargets2()
    {
        if (RoamCheckDelay > 0f) { RoamCheckDelay -= Time.deltaTime; return; } else { RoamCheckDelay = Random.Range(1f, 2f); RoamCheckDelay -= Time.deltaTime; }
        if (EnemyToAttack == null) // Is the unit on it's way somewhere and do we have a target to attack?
        {
            int layerMask = 1 << (int)enemyLayer; // Bit shift our layer variable. This tells the unit which layer to consider an enemy.
            float distance = 0f;
            GameObject targettoattack = null;
            foreach(GameObject gobject in GameObject.FindGameObjectsWithTag("Vessel"))
            {
                if(gobject == null) { return; }
                if(gobject.layer == (int)enemyLayer)
                {
                    var dist = Vector3.Distance(gobject.transform.position, transform.position);
                    if (dist < AttackDistance && dist > distance)
                    {
                        distance = dist;
                        targettoattack = gobject;
                    }
                }
            }
            EnemyToAttack = targettoattack;
        }
    }

    public void Damage(float damageToDeal) // Public method to deal damage to this unit.
    {
        if(VesselHealth <= 0f) // Death
        {
            AudioSource source = GetComponent<AudioSource>();
            source.volume = 100f;
            source.PlayOneShot(vesselExplosionSFX);
            MeshCollider meshCollider = GetComponentInChildren<MeshCollider>();
            meshCollider.enabled = false;
            moveOrderGiven = true;
            rb.isKinematic = true;
            GameObject death = Instantiate(DeathPrefab, transform.position, transform.rotation);
            Destroy(gameObject, 2f);
        }
        VesselHealth -= damageToDeal; // else deal damage.
    }
}
