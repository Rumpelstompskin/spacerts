﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCursorControl;

[RequireComponent(typeof(Raycaster))]
[RequireComponent(typeof(PlayerData))]
public class AttackOrderSystem : Raycaster
{
    private PlayerData playerData;
    [SerializeField] KeyCode moveCode;
    [SerializeField] private Camera m_Cam;
    [SerializeField] GameObject movementCanvasPrefab;
    [SerializeField] GameObject movementDistanceText;

    Vector3 clickPoint = Vector3.zero;
    Vector2 originalMousePos;
    bool mousePosSaved = false;
    Vector3 clickPointY = Vector3.zero;
    public bool MovementUIActive
    {
        get
        {
            return moveUIActive;
        }
    }
    private bool moveUIActive = false;
    GameObject[] moveUIcanvas;
    LineRenderer lineRenderer;
    float yOffset;
    //Bounds bounds;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
        moveUIcanvas = new GameObject[3];
        playerData = GetComponent<PlayerData>();
    }

    private void Update()
    {
        if(moveUIActive != true)
        {
            MovementUIStep1();
        }
        MovementUIStep2();
        MovementUIStep3();
        if (Input.GetMouseButtonDown(1) && moveUIActive)
        {
            MovementUICancel();
        }
    }

    public Vector3 SelectedUnitCenter
    {
        get
        {
            return GetCenterPoint();
        }
    }
    Vector3 GetCenterPoint()
    {
        if (playerData._playerSelectedTargets[0] == null)
        {
            for (int i = 0; i < playerData._playerSelectedTargets.Count; i++)
            {
                if(playerData._playerSelectedTargets[i] != null)
                {
                    playerData._playerSelectedTargets[0] = playerData._playerSelectedTargets[i];
                    playerData._playerSelectedTargets[i] = null;
                    break;
                }
            }
        }
        if (playerData._playerSelectedTargets.Count == 1)
        {
            return playerData._playerSelectedTargets[0].transform.position;
        }
        var bound = new Bounds(playerData._playerSelectedTargets[0].transform.position, Vector3.zero);

        for (int i = 0; i < playerData._playerSelectedTargets.Count; i++)
        {
            if (playerData._playerSelectedTargets[i] != null)
            {
                 bound.Encapsulate(playerData._playerSelectedTargets[i].transform.position);
            }
        }

        return bound.center;
    }

    private void MovementUIStep1()
    {
        if (Input.GetKeyDown(moveCode) && playerData._playerSelectedTargets[0] != null && playerData._playerSelectedTargets != null)
        {
            clickPoint = GetClickedPosition(m_Cam, GetCenterPoint());
            if (clickPoint != Vector3.zero)
            {
                lineRenderer.enabled = true;

                GameObject movementCanvas = Instantiate(movementCanvasPrefab, GetCenterPoint(), transform.rotation);
                movementCanvas.transform.localScale = movementCanvas.transform.localScale / 20f;
                moveUIcanvas[0] = movementCanvas;
                lineRenderer.SetPosition(0, GetCenterPoint());
                Vector3 newDir = Vector3.RotateTowards(transform.forward, Vector3.up, 100f * Time.deltaTime, 0.0f);
                movementCanvas.transform.rotation = Quaternion.LookRotation(newDir);

                GameObject movementCanvas2 = Instantiate(movementCanvasPrefab, clickPoint, transform.rotation);
                moveUIcanvas[1] = movementCanvas2;
                movementCanvas2.transform.localScale = movementCanvas2.transform.localScale / 20f;
                movementCanvas2.transform.rotation = Quaternion.LookRotation(newDir);
                lineRenderer.SetPosition(1, movementCanvas2.transform.position);

                GameObject movementCanvas3 = Instantiate(movementCanvasPrefab, clickPoint, transform.rotation);
                moveUIcanvas[2] = movementCanvas3;
                movementCanvas3.transform.localScale = movementCanvas3.transform.localScale / 20f;
                movementCanvas3.transform.rotation = Quaternion.LookRotation(newDir);
                lineRenderer.SetPosition(2, movementCanvas3.transform.position);
                moveUIActive = true;
            }
        }
    }

    private void MovementUIStep2()
    {
        if (moveUIActive == true)
        {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    if (mousePosSaved == false)
                    {
                        mousePosSaved = true;
                        originalMousePos = Input.mousePosition;
                    }

                    Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                    float vertical = Input.GetAxis("Mouse Y");
                    yOffset += vertical;
                    clickPointY = new Vector3(clickPoint.x, clickPoint.y + yOffset, clickPoint.z);
                    moveUIcanvas[2].transform.position = clickPointY;
                    lineRenderer.SetPosition(2, clickPointY);

                }
            else
            {
                    

            if (mousePosSaved == true)
                {
                Cursor.lockState = CursorLockMode.None;
                CursorControl.SetLocalCursorPos(originalMousePos);
                mousePosSaved = false;
                }

                clickPoint = GetClickedPosition(m_Cam, GetCenterPoint());
                
                if (clickPoint != Vector3.zero)
                {
                    moveUIcanvas[0].transform.position = GetCenterPoint(); // Was bounds.center
                    moveUIcanvas[1].transform.position = clickPoint;
                    clickPointY = new Vector3(clickPoint.x, clickPoint.y + yOffset, clickPoint.z);
                    moveUIcanvas[2].transform.position = clickPointY;
                    lineRenderer.SetPosition(0, GetCenterPoint());
                    lineRenderer.SetPosition(1, clickPoint);
                    lineRenderer.SetPosition(2, clickPointY);
                    Cursor.visible = true;
                }
            }
            
        }
    }

    private void MovementUIStep3()
    {
        if (Input.GetMouseButtonUp(0) && moveUIActive)
        {
            Cursor.visible = true;
            MoveUnits(clickPointY);
            MovementUICancel();
        }
    }

    private void MovementUICancel()
    {
        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            Destroy(moveUIcanvas[i]);
            lineRenderer.SetPosition(i, Vector3.zero);
        }
        yOffset = 0f;
        mousePosSaved = false;
        moveUIActive = false;
        lineRenderer.enabled = false;
    }

    private void MoveUnits(Vector3 moveCoords)
    {
        /*
        foreach (GameObject selectedUnit in playerData._playerSelectedTargets)
        {
            if (playerData._playerOwnedTargets.Contains(selectedUnit))
            {
                if (selectedUnit != null)
                {
                    UnitAI unitAI = selectedUnit.GetComponent<UnitAI>();
                    unitAI.TargetPos = moveCoords;
                    unitAI.moveOrderGiven = true;
                }
            }
        }
        */
        float addition = 0f;
        for (int i = 0; i < playerData._playerSelectedTargets.Count; i++)
        {
            Vector3 offset = new Vector3(0 + addition, 0, 0);
            if (playerData._playerOwnedTargets.Contains(playerData._playerSelectedTargets[i]))
            {
                if(playerData._playerSelectedTargets[i] != null)
                {
                    UnitAI unitAI = playerData._playerSelectedTargets[i].GetComponent<UnitAI>();
                    unitAI.TargetPos = moveCoords + offset;
                    unitAI.moveOrderGiven = false;
                    unitAI.attackorderGiven = true;
                    addition += 5f;
                }
            }
        }
    }
}
