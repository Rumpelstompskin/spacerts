﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCursorControl;

[RequireComponent(typeof(Raycaster))]
[RequireComponent(typeof(PlayerData))]
public class PlayerCamera : Raycaster
{
    PlayerData data;
    [SerializeField] Texture2D StandardCursor;
    [SerializeField] Texture2D EnemyCursor;
    [SerializeField] Texture2D SelectionCursor;
    [SerializeField] Vector2 CursorHotSpot = new Vector2(16, 16);

    [SerializeField] Camera m_Cam;

    [SerializeField]
    private float scrollWheelSpeed = 10f;
    [SerializeField]
    private float cameraMoveSpeed = 200f;
    
    public Layer[] layerPriorities =
    {
        Layer.Enemy,
        Layer.Friendly,
        Layer.Enviroment,
        Layer.RaycastEndStop
    };

    public Layer previousLayer;

    public delegate void LayerChange(Layer layer);
    public event LayerChange changeLayers;

    private void Awake()
    {
        data = GetComponent<PlayerData>();
        changeLayers += LayerChanger;
    }

    private void LateUpdate()
    {
        if (data._playerSelectedTargets.Count >= 1 && data._playerSelectedTargets[0] != null)
        {
            var newPos = GetComponent<MoveOrderSystem>().SelectedUnitCenter;
            if (newPos != null)
            {
                if (transform.position == newPos) { return; }
                transform.position = Vector3.MoveTowards(transform.position, newPos, cameraMoveSpeed * Time.deltaTime);
            }
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(1)) // TODO Make cursor invisible and locked to the screen.
        {
            float horizontal = Input.GetAxis("Mouse X");
            float vertical = Input.GetAxis("Mouse Y");

            transform.Rotate(new Vector3(-vertical, 0, 0), Space.Self);
            transform.Rotate(new Vector3(0, horizontal, 0), Space.World);
        }
        
        m_Cam.transform.localPosition = new Vector3(m_Cam.transform.localPosition.x, m_Cam.transform.localPosition.y - Input.GetAxis("Mouse ScrollWheel") * scrollWheelSpeed * Time.deltaTime, m_Cam.transform.localPosition.z + Input.GetAxis("Mouse ScrollWheel") * scrollWheelSpeed * Time.deltaTime);
    }

    private void Update()
    {
        foreach (Layer layer in layerPriorities)
        {
            var layerHit = RaycastForLayer(m_Cam, layer);
            if (layerHit.HasValue) // TODO Check if this fires if we are already on the same layer. Add condition to avoid if so.
            {
                if (layer != previousLayer && layerHit.Value.transform.gameObject.layer != LayerMask.NameToLayer(previousLayer.ToString())) 
                {
                    previousLayer = layer;
                    changeLayers(layer);
                }
                break;
            } else
            {
                if(previousLayer != Layer.RaycastEndStop)
                {
                    previousLayer = Layer.RaycastEndStop;
                    changeLayers(Layer.RaycastEndStop);
                }
            }
        }

        if(previousLayer == Layer.Enemy)
        {
            if (Input.GetMouseButtonDown(1))
            {
                foreach(GameObject vessel in data._playerSelectedTargets)
                {
                    if(vessel != null)
                    {
                        UnitAI ai = vessel.GetComponent<UnitAI>();
                        ai.EnemyToAttack = GetClickedGameObject(m_Cam);
                    }
                }
            }
        }
    }
    

    private void LayerChanger(Layer layer)
    {
        switch (layer)
        {
            case Layer.Enemy:
                Cursor.SetCursor(EnemyCursor, CursorHotSpot, CursorMode.Auto); print("Enemy layer");
                break;
            case Layer.Friendly:
                Cursor.SetCursor(SelectionCursor, CursorHotSpot, CursorMode.Auto); print("Friendly layer");
                break;
            case Layer.Enviroment:
                break;
            case Layer.RaycastEndStop:
                Cursor.SetCursor(StandardCursor, CursorHotSpot, CursorMode.Auto); print("Neutral layer");
                break;
        }
    }
}
