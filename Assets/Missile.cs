﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    [SerializeField] AudioClip explosionSFX;
    [SerializeField] GameObject MissileModel;
    [SerializeField] GameObject explosionprefab;

    public GameObject g_Target = null;
    public GameObject shooter = null;
    public float TrackingRotation = 10f;
    public float MissileSpeed = 500f;

    [SerializeField] private float flightTime = 10f;

    bool off = false;

    private void Awake()
    {
        Destroy(gameObject, flightTime); // This ensures that the missile will not exist in the hierachy forever.
    }

    private void FixedUpdate()
    {
        if (g_Target != null)
        {
            //GoToTarget2(g_Target, shooter, GetComponent<Rigidbody>(), 50000f, MissileSpeed);
            MoveUnit(g_Target.transform.position, TrackingRotation, MissileSpeed);
        } else if (g_Target == null && off == false)
        {
            off = true;
            Die();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        UnitAI ai = collision.gameObject.GetComponent<UnitAI>();
        if(ai != null)
        {
            if(shooter != null)
            {
                ai.Damage(shooter.GetComponent<UnitAI>().AttackDamage);
            }
        }
        Die();
    }

    private void Die()
    {
        AudioSource source = GetComponent<AudioSource>();
        source.PlayOneShot(explosionSFX);
        ParticleSystem particleSystem = GetComponentInChildren<ParticleSystem>();
        if(particleSystem != null)
        {
            particleSystem.gameObject.SetActive(false);
        }
        off = true;
        Instantiate(explosionprefab, transform.position, transform.rotation);
        MeshCollider meshCollider = GetComponentInChildren<MeshCollider>();
        if (meshCollider != null) { meshCollider.enabled = false; }
        MissileModel.SetActive(false);
        Destroy(gameObject, 2f);
    }

    public void MoveUnit(Vector3 targetPos, float rotationSpeed, float moveSpeed) // Deprecated
    {
        if (targetPos != null && off == false)
        {
            Vector3 direction = targetPos - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, direction, rotationSpeed * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);

            transform.position = Vector3.MoveTowards(transform.position, targetPos, moveSpeed * Time.deltaTime);
        }
    }
}
