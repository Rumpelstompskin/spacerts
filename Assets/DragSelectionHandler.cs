﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragSelectionHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler//, IPointerClickHandler
{

    [SerializeField] Image selectionBoxImage;

    Vector2 startPostion;
    Rect selectionRect;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!Input.GetMouseButton(1) && !Input.GetMouseButton(2))
        {
            if (!Input.GetKey(KeyCode.LeftControl))
            {
                UnitData.DeselectAll(new BaseEventData(EventSystem.current));
            }
            selectionBoxImage.gameObject.SetActive(true);
            startPostion = eventData.position;
            selectionRect = new Rect();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!Input.GetMouseButton(1) && !Input.GetMouseButton(2))
        {
            if (eventData.position.x < startPostion.x)
            {
                selectionRect.xMin = eventData.position.x;
                selectionRect.xMax = startPostion.x;
            }
            else
            {
                selectionRect.xMin = startPostion.x;
                selectionRect.xMax = eventData.position.x;
            }
            if (eventData.position.y < startPostion.y)
            {
                selectionRect.yMin = eventData.position.y;
                selectionRect.yMax = startPostion.y;
            }
            else
            {
                selectionRect.yMin = startPostion.y;
                selectionRect.yMax = eventData.position.y;
            }

            selectionBoxImage.rectTransform.offsetMin = selectionRect.min;
            selectionBoxImage.rectTransform.offsetMax = selectionRect.max;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!Input.GetMouseButton(1) && !Input.GetMouseButton(2))
        {
            selectionBoxImage.gameObject.SetActive(false);
            foreach (UnitData units in UnitData.allUnits)
            {
                if (units != null)
                    if (selectionRect.Contains(Camera.main.WorldToScreenPoint(units.transform.position)))
                    {
                        units.OnSelect(eventData);
                    }
            }
        }
    }

    
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!Input.GetMouseButton(1) && !Input.GetMouseButton(2))
        {
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, results);

            float MyDistance = 0f;

            foreach (RaycastResult result in results)
            {
                if (result.gameObject == gameObject)
                {
                    MyDistance = result.distance;
                    break;
                }
            }

            GameObject nextObject = null;
            float maxDistance = Mathf.Infinity;

            foreach (RaycastResult result in results)
            {
                if (result.distance > MyDistance && result.distance < maxDistance)
                {
                    nextObject = result.gameObject;
                    maxDistance = result.distance;
                }
            }

            if (nextObject)
            {
                ExecuteEvents.Execute<IPointerClickHandler>(nextObject, eventData, (x, y) => { x.OnPointerClick((PointerEventData)y); });
            }
            else if (!eventData.dragging)
            {
                UnitData.DeselectAll(eventData);
            }
        }
    }
    
}
