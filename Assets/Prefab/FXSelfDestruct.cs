﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXSelfDestruct : MonoBehaviour
{
    [SerializeField] float timeToDeath = 10f;

    private void Awake()
    {
        Destroy(gameObject, timeToDeath);
    }
}
