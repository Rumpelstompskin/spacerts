﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public void MoveUnit(Vector3 targetPos, float rotationSpeed, float moveSpeed) // Used by projectiles
    {
        if(targetPos != null)
        {
            if(transform.position == targetPos) { return; }
            Vector3 direction = targetPos - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, direction, rotationSpeed * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);

            transform.position = Vector3.MoveTowards(transform.position,transform.forward + targetPos, moveSpeed * Time.deltaTime);
        }
    } 
    
    public void MoveUnitRB(Vector3 targetPos, float rotationSpeed, float moveSpeed, Rigidbody rb) // TODO Add deceleration when nearing target.
    {
        if (targetPos != null) // We make sure that we have a destination.
        {
            if (transform.position == targetPos) { return; } // If our position is the same as our destination.
            Vector3 direction = targetPos - transform.position; // stores our direction.
            Vector3 newDir = Vector3.RotateTowards(transform.forward, direction, rotationSpeed * Time.deltaTime, 0.0f); // Vector3.RotateTowards calculates the rotational trajectory.
            transform.rotation = Quaternion.LookRotation(newDir); // We then apply this calculation.

            rb.AddRelativeForce(Vector3.forward * moveSpeed * Time.deltaTime, ForceMode.Acceleration); // Apply thrust forward.
        }
    }

    public void AttackNearbyEnemy(GameObject enemyToAttack, GameObject attackingUnit, GameObject shotPrefab, float attackRange, float attackDamage)
    {
        if(enemyToAttack != null)
        {
            
        }
    }

    public void MissileAttack(GameObject enemyToAttack, GameObject attackingUnit, Rigidbody attackingUnitRB, GameObject shotPrefab, GameObject shotPoint, float attackRange, float attackDamage)
    {
        if (enemyToAttack != null)
        {
            GameObject missile = Instantiate(shotPrefab, shotPoint.transform.position, transform.rotation);
            Rigidbody missileRB = missile.GetComponent<Rigidbody>();
            //missileRB.velocity = attackingUnitRB.velocity;
            //missileRB.AddRelativeForce(Vector3.down * 2f);
            Missile script = missile.GetComponent<Missile>();
            script.g_Target = enemyToAttack;
            script.shooter = attackingUnit;
        }
    }
}
