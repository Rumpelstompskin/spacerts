﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    PlayerCamera playerCamera;

    private void Awake()
    {
        playerCamera = GetComponent<PlayerCamera>();
    }

    public Vector3 GetClickedPosition(Camera cam, Vector3 unitPos)
    {
        Vector3 clickedPosition;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        Plane plane = new Plane(Vector3.up, unitPos); // Was Vector3.zero

        float distance = 1000f;
        if (plane.Raycast(ray, out distance))
        {
            clickedPosition = ray.GetPoint(distance);
            return clickedPosition;
        } else
        {
            return Vector3.zero;
        }
    }

    public GameObject GetClickedGameObject(Camera cam)
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
        {
            return hit.collider.gameObject;
        } else
        {
            return null;
        }
    }

    public RaycastHit? RaycastForLayer(Camera cam, Layer layer) // TODO Figure out a way to merge both raycast actions.
    {
        int layerMask = 1 << (int)layer; // See Unity docs for mask formation
        Ray ray = cam.ScreenPointToRay(Input.mousePosition); // stores a ray cast into the screen.

        RaycastHit hit; // used as an out parameter
        bool hasHit = Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask); // checks if we hit a layer as a bool.
        if (hasHit) // Did we hit?
        {
            return hit; // If so return the value of our RaycastHit hit get method.
        }
        return null; // if not return null.
    }
}