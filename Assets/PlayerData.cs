﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    [SerializeField] PlayerCamera _playerCameraScript;

    public List<GameObject> _playerSelectedTargets;
    public List<GameObject> _playerOwnedTargets;

    private void Awake()
    {
        _playerSelectedTargets = new List<GameObject>();
        //_playerSelectedTargets = new List<GameObject>();
        //_playerOwnedTargets = new List<GameObject>();
        _playerCameraScript = GetComponent<PlayerCamera>();
        //StartCoroutine(ListClearLoop());
    }

    private void Update()
    {

    }

    IEnumerator ListClearLoop()
    {
        while (true)
        {
            for (int i = 0; i < _playerSelectedTargets.Count; i++)
            {
                yield return new WaitForFixedUpdate();
                if (_playerSelectedTargets[i] == null)
                {
                    _playerSelectedTargets.RemoveAt(i);
                }
            }
            
            for (int z = 0; z < _playerOwnedTargets.Count - 1; z++)
            {
                yield return new WaitForFixedUpdate();
                if (_playerSelectedTargets[z] == null)
                {
                    //_playerSelectedTargets.RemoveAt(z);
                }
            }
            yield return new WaitForSeconds(5f);
        }
    }
}
