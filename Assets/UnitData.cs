﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitData : MonoBehaviour, ISelectHandler, IDeselectHandler, IPointerClickHandler
{
    PlayerData data;
    MoveOrderSystem moveOrderSystem;

    public static HashSet<UnitData> allUnits = new HashSet<UnitData>();
    public static HashSet<UnitData> selectedUnits = new HashSet<UnitData>();

    [SerializeField] GameObject selectionUI;
    GameObject instantiatedUI;

    private void Awake()
    {
        data = Camera.main.GetComponentInParent<PlayerData>();
        moveOrderSystem = Camera.main.GetComponentInParent<MoveOrderSystem>();
        allUnits.Add(this);
    }

    private void Update()
    {
        if(instantiatedUI != null)
        {
            instantiatedUI.transform.position = transform.position;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.dragging) { return; }
        if (!Input.GetKey(KeyCode.LeftControl))
        {
            DeselectAll(new BaseEventData(EventSystem.current));
        }
        OnSelect(eventData);
    }

    public void OnSelect(BaseEventData eventData)
    {
        //if (selectedUnits.Contains(this)) { return; }
        //if (data._playerSelectedTargets.Contains(gameObject)) { return; }
        if (!data._playerSelectedTargets.Contains(gameObject))
        {
            data._playerSelectedTargets.Add(gameObject);
            if (GetComponent<UnitAI>().enemyLayer == Layer.Friendly && Input.GetKey(KeyCode.LeftShift))
            {
                selectedUnits.Add(this);
                GameObject ui = Instantiate(selectionUI, transform.position, selectionUI.transform.rotation);
                instantiatedUI = ui;
            }
            if (GetComponent<UnitAI>().enemyLayer == Layer.Enemy && !Input.GetKey(KeyCode.LeftShift))
            {
                selectedUnits.Add(this);
                GameObject ui = Instantiate(selectionUI, transform.position, selectionUI.transform.rotation);
                instantiatedUI = ui;
            }
        }
    }

    public void OnDeselect(BaseEventData eventData)
    {
        if(moveOrderSystem.MovementUIActive == false)
        {
            Destroy(instantiatedUI);
            data._playerSelectedTargets.Clear();
        }
    }

    public static void DeselectAll(BaseEventData eventData)
    {
        foreach (UnitData selectedUnit in selectedUnits)
        {
            selectedUnit.OnDeselect(eventData);
        }
        selectedUnits.Clear();
    }
}
